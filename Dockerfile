FROM python:latest
RUN apt-get -y update 
ADD . /djangoapp
WORKDIR /djangoapp
RUN pip install -r requirements.txt
EXPOSE 8000
ENTRYPOINT ["gunicorn", "-b", ":8000", "dockerdjango.wsgi", "--log-file", "-"]
